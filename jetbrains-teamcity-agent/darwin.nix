#
# Copyright © 2023 framebunker ApS
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
# 
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

{ lib, pkgs, config, ... }:
let
  cfg = config.services.jetbrains.teamcity.agent;

  names = {
    installAll = "teamcity-agent-install-all";
    install = agent: "teamcity-agent-${agent.agentName}-install";
    reinstall = agent: "teamcity-agent-${agent.agentName}-reinstall";
    reconfigure = agent: "teamcity-agent-${agent.agentName}-reconfigure";
    run = agent: "teamcity-agent-${agent.agentName}-run";
  };
  labels = {
    install = agent: "teamcity.agent.${agent.agentName}.install";
    reinstall = agent: "teamcity.agent.${agent.agentName}.reinstall";
    reconfigure = agent: "teamcity.agent.${agent.agentName}.reconfigure";
    run = agent: "teamcity.agent.${agent.agentName}.run";
  };
  scriptLaunchPath = agent: "/Users/${agent.agentUserName}/.nix-profile/bin";
  finalEnvironment = agent: (lib.mkMerge [
    agent.environment
    {
      PATH = lib.mkForce (
        (lib.strings.concatStrings (builtins.map (p: (builtins.toString p) + ":") agent.path)) +
        (
          if builtins.hasAttr "PATH" agent.environment
          then (builtins.getAttr "PATH" agent.environment)
          else ""
        )
      );
    }
  ]);
in
with lib;
with (import ./utilities.nix
{
  lib = lib;
  pkgs = pkgs;
  config = config;
});
{
  imports = [ ./options.nix ];

  config = mkIf (cfg.enable && !pkgs.hostPlatform.isLinux)
  {
    home.packages = (lists.flatten (builtins.map (agent:
    let
      paths = agentPaths agent;
      agentScriptPath = "${paths.install}/bin/agent.sh";
      agentInstallZip = "buildAgentFull.zip";
      agentInstallURL = "http://${agent.serverURL}/update/${agentInstallZip}";
      agentPropertiesPath = "${paths.configuration}/buildAgent.properties";
      agentBasePropertiesPath = "${paths.configuration}/buildAgent.dist.properties";
    in
    with pkgs;
    [
      (writeShellApplication
      {
        name = names.install agent;
        runtimeInputs = [ curl ];
        text = ''
          wait_for_url()
          {
            echo -n "Wait for URL $1"
            while ! curl --output /dev/null --silent --head --fail "$1"; do
              sleep 0.5 && echo -n .
            done
            echo
          }
      
          if [ -e ${agentScriptPath} ] && [ -e ${agentPropertiesPath} ]; then
            echo "Agent install located at ${agentScriptPath} and ${agentPropertiesPath}. No work to do here."
            exit 0
          fi

          wait_for_url ${agentInstallURL}

          ${scriptLaunchPath agent}/${names.reinstall agent}
        '';
      })

      (writeShellApplication
      {
      	name = names.reinstall agent;
      	runtimeInputs = [ wget unzip ];
        text = ''
          installPath="${paths.install}"

          echo "(Re)installing agent ${agent.agentName} at $installPath..."

          rm -r $installPath 2>/dev/null || true
          mkdir -p $installPath
          cd $installPath

          echo "Fetching build agent install from server at ${agent.serverURL}"
          wget ${agentInstallURL}
          unzip ${agentInstallZip}
          rm ${agentInstallZip}

          if [ ! -e ${agentPropertiesPath} ]; then
            echo "Creating new agent configuration at ${agentPropertiesPath}"

            mkdir -p "${paths.configuration}" 2>/dev/null || true
            mv conf/*.* ${paths.configuration}/
            
            ${scriptLaunchPath agent}/${names.reconfigure agent}
          else
            echo "Reusing existing agent configuration at ${agentPropertiesPath}"
          fi

          echo "Securing paths owned by ${cfg.pathOwner}:"
          echo " - $installPath"
          echo " - ${paths.work}"
          echo " - ${paths.log}"
          echo " - ${paths.temp}"

          mkdir -p "${paths.work}" 2>/dev/null || true
          chown -R ${cfg.pathOwner} "${paths.work}"
          mkdir -p "${paths.log}" 2>/dev/null || true
          chown -R ${cfg.pathOwner} "${paths.log}"
          mkdir -p "${paths.temp}" 2>/dev/null || true
          chown -R ${cfg.pathOwner} "${paths.temp}"

          rm -r conf work logs temp 2>/dev/null || true
          chmod -R 770 $installPath
          ln -s ${paths.configuration} conf
          ln -s ${paths.work} work
          ln -s ${paths.log} logs
          ln -s ${paths.temp} temp
          ln -s ${cfg.javaPackage} jre

          chown -R ${cfg.pathOwner} "$installPath"

          echo "Install complete"
        '';
      })

      (writeShellApplication
      {
        name = names.reconfigure agent;
        runtimeInputs = [ gnused ];
        text = ''
          echo "Reconfiguring agent ${agent.agentName} at ${agentPropertiesPath}"

          cp ${agentBasePropertiesPath} ${agentPropertiesPath}

          echo "Setting name to ${agent.agentName}..."
          sed -i 's/^name=/name=${agent.agentName}/g' ${agentPropertiesPath}

          echo "Setting URL to ${agent.serverURL}..."
          sed -i 's/^serverUrl=http:\/\/localhost:8111\//serverUrl=http:\/\/${agent.serverURL}/g' ${agentPropertiesPath}

          echo "Adding env._..."
          printf "\nenv._=${agentScriptPath}\n" >> ${agentPropertiesPath}

          chmod -R 770 ${paths.configuration}
          chown -R ${cfg.pathOwner} ${paths.configuration}

          echo
          echo "Result:"
          cat ${agentPropertiesPath}

          echo
          echo "Reconfigure complete"
        '';
      })
    ]) cfg.agents)) ++
    [(pkgs.writeScriptBin (names.installAll) (
      strings.concatStrings (builtins.map (agent: "${scriptLaunchPath agent}/${names.install agent}\n") cfg.agents)
    ))];
  
    launchd =
    {
      enable = true;
      agents = listToAttrs (lists.flatten (builtins.map (agent:
      let
        paths = agentPaths agent;
      in
      [
        {
          name = names.run agent;
          value = {
            enable = true;
            config = {
              Label = labels.run agent;
    
              WorkingDirectory = paths.install;
    
              RunAtLoad = true;
              KeepAlive = true;
    
              ProcessType = "Interactive";
              LowPriorityIO = false;
#             LowPriorityBackgroundIO = false; # omission in home-manager
    
              ProgramArguments = [
                "/bin/bash"
                "--login"
                "-c"
                "bin/agent.sh run"
              ];

              EnvironmentVariables = finalEnvironment agent;

              StandardErrorPath = /. + paths.install + "/logs/launchd.run.err.log";
              StandardOutPath = /. + paths.install + "/logs/launchd.run.out.log";
            };
          };
        }
      ] ) cfg.agents));
    };
  };
}
