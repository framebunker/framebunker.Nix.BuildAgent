#
# Copyright © 2023 framebunker ApS
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
# 
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

{ lib, pkgs, config, ... }:
let
  cfg = config.services.jetbrains.teamcity.agent;
  
  defaultString = value: default: if value == "" then default else value;
  overriddenPath = set: default: postfix: defaultString set (default + "/" + postfix);
in
with lib; rec
{
  agentPaths = agent:
  let
    set = agent.paths;
    default = cfg.defaultPaths;
    postfix = agent.agentName;
  in
  {
    install = overriddenPath set.install default.install postfix;
    log = overriddenPath set.log default.log postfix;
    configuration = overriddenPath set.configuration (defaultString default.configuration (default.bulk + "/teamcity-agent/configuration")) postfix;
    work = overriddenPath set.work (defaultString default.work (default.bulk + "/teamcity-agent/work")) postfix;
    temp = overriddenPath set.temp (defaultString default.temp (default.bulk + "/teamcity-agent/temp")) postfix;
  };
}
