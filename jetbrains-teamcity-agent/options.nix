#
# Copyright © 2023 framebunker ApS
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
# 
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

{ lib, pkgs, config, ... }:
let
  cfg = config.services.jetbrains.teamcity.agent;

  agentOptions.options = with lib; {
    serverURL = mkOption {
      type = types.str;
      description = "URL (sans protocol) of the TeamCity server to which the agent should be connected";
      default = "localhost";
    };
    agentName = mkOption {
      type = types.str;
      description = "Name of the agent as listed in TeamCity";
    };
    agentUserID = mkOption {
      type = types.str;
      description = "ID of the user account under which to run the agent service";
      default = "1000";
    };
    agentUserName = mkOption {
      type = types.str;
      description = "Name of the user account under which to run the agent service";
      default = "build";
    };
    paths = {
      install = mkOption {
        type = types.str;
        description = "Path to directory where agent binaries should be written";
        default = "";
      };
      configuration = mkOption {
        type = types.str;
        description = "Path to directory where configuration for the agent should be written";
        default = "";
      };
      work = mkOption {
        type = types.str;
        description = "Path to working directory for the agent";
        default = "";
      };
      log = mkOption {
        type = types.str;
        description = "Path to logs directory for the agent";
        default = "";
      };
      temp = mkOption {
        type = types.str;
        description = "Path to temp directory for the agent";
        default = "";
      };
    };
    environment = mkOption {
      type = with types; attrsOf (nullOr (oneOf [ str path package ]));
      description = "Environment variables set for the build agent service";
      default = {};
    };
    path = mkOption {
      type = with types; listOf (nullOr (oneOf [ str path package ]));
      description = "Packages added to the agents PATH environment variable. Both the bin and sbin subdirectories of each package are added.";
      default = [];
    };
  };
in
with lib;
{
  options.services.jetbrains.teamcity.agent = {
    enable = mkEnableOption "Build agent for JetBrains TeamCity";
    javaPackage = mkOption {
      type = types.package;
      description = "Java package used to run the agents";
      default = pkgs.jdk11;
    };
    defaultPaths = {
      install = mkOption {
        type = types.str;
        description = "Agents with no install path specified will be configured to use this path with '/agentName' appended";
        default = "/var/lib/teamcity-agent";
      };
      log = mkOption {
        type = types.str;
        description = "Agents with no log path specified will be configured to use this path with '/agentName' appended";
        default = "/var/log/teamcity-agent";
      };
      bulk = mkOption {
        type = types.str;
        description = "";
        default = "/tmp";
      };
      configuration = mkOption {
        type = types.str;
        description = ''
          Agents with no configuration path specified will be configured to use this path with '/<agentName>' appended or
          the bulk path with '/teamcity-agent/configuration/<agentName>' appended
       '';
        default = "";
      };
      work = mkOption {
        type = types.str;
        description = ''
          Agents with no work path specified will be configured to use this path with '/<agentName>' appended or
          the bulk path with '/teamcity-agent/work/<agentName>' appended
        '';
        default = "";
      };
      temp = mkOption {
        type = types.str;
        description = ''
          Agents with no temp path specified will be configured to use this path with '/<agentName>' appended or
          the bulk path with '/teamcity-agent/temp/<agentName>' appended
        '';
        default = "";
      };
    };
    pathOwner = mkOption {
      type = types.str;
      description = "User set as owner of all paths specified as default or per-agent";
      default = "1000";
    };
    agents = mkOption {
      type = with types; listOf (submodule agentOptions);
      description = "";
      default = [];
    };
  };
}
