#
# Copyright © 2023 framebunker ApS
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
# 
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

{ lib, pkgs, config, ... }:
let
  cfg = config.services.jetbrains.teamcity.agent;
in
with lib;
with (import ./utilities.nix
{
  lib = lib;
  pkgs = pkgs;
  config = config;
});
{
  imports = [ ./options.nix ];

  config = mkIf cfg.enable
  {
    environment.systemPackages = [];

    systemd.tmpfiles.rules = builtins.map (
      path: "d ${path} 0777 ${cfg.pathOwner} users"
    ) (lists.flatten ((builtins.map (agent: (builtins.attrValues (agentPaths agent))) cfg.agents)));

    systemd.services = listToAttrs (lists.flatten (builtins.map (agent:
      let
        paths = agentPaths agent;
        agentScriptPath = "${paths.install}/bin/agent.sh";
        agentInstallZip = "buildAgentFull.zip";
        agentInstallURL = "http://${agent.serverURL}/update/${agentInstallZip}";
        agentPropertiesPath = "${paths.configuration}/buildAgent.properties";
        agentBasePropertiesPath = "${paths.configuration}/buildAgent.dist.properties";
      in
      [
        {
          name = "teamcity-agent-${agent.agentName}-install";
          value = {
            serviceConfig = {
              Type = "oneshot";
              User = "root";
            };
            after = [ "network-online.target" ];
            script = ''
              wait_for_url()
              {
                echo -n "Wait for URL $1"
                while ! ${pkgs.curl}/bin/curl --output /dev/null --silent --head --fail $1; do
                  sleep 0.5 && echo -n .
                done
                echo
              }
            
              if [ -e ${agentScriptPath} ] && [ -e ${agentPropertiesPath} ]; then
                echo "Agent install located at ${agentScriptPath} and ${agentPropertiesPath}. No work to do here."
                exit 0
              fi
      
              wait_for_url ${agentInstallURL}
      
              systemctl restart teamcity-agent-${agent.agentName}-reinstall
            '';
          };
        }
        {
          name = "teamcity-agent-${agent.agentName}-reinstall";
          value = {
            serviceConfig = {
              Type = "oneshot";
              User = "root";
            };
            path = with pkgs; [
              wget
              unzip
            ];
            script = ''
              installPath="${paths.install}"
    
              echo "(Re)installing agent ${agent.agentName} at $installPath..."
    
              rm -r $installPath 2>/dev/null || true
              mkdir -p $installPath
              cd $installPath
    
              echo "Fetching build agent install from server at ${agent.serverURL}"
              wget ${agentInstallURL}
              unzip ${agentInstallZip}
              rm ${agentInstallZip}
    
              if [ ! -e ${agentPropertiesPath} ]; then
                echo "Creating new agent configuration at ${agentPropertiesPath}"
    
                mv conf/*.* ${paths.configuration}/
                
                systemctl restart --wait teamcity-agent-${agent.agentName}-reconfigure
              else
                echo "Reusing existing agent configuration at ${agentPropertiesPath}"
              fi
    
              rm -r conf work logs temp 2>/dev/null || true
              chmod -R 770 $installPath
              ln -s ${paths.configuration} conf
              ln -s ${paths.work} work
              ln -s ${paths.log} logs
              ln -s ${paths.temp} temp
              ln -s ${cfg.javaPackage} jre
    
              chown -R ${cfg.pathOwner} $installPath
            '';
          };
        }
        {
          name = "teamcity-agent-${agent.agentName}-reconfigure";
          value = {
            serviceConfig = {
              Type = "oneshot";
              User = "root";
            };
            script = ''
              echo "Reconfiguring agent ${agent.agentName} at ${agentPropertiesPath}"
            
              cp ${agentBasePropertiesPath} ${agentPropertiesPath}

              echo "Setting name to ${agent.agentName}..."
              sed -i 's/^name=/name=${agent.agentName}/g' ${agentPropertiesPath}
              
              echo "Setting URL to ${agent.serverURL}..."
              sed -i 's/^serverUrl=http:\/\/localhost:8111\//serverUrl=http:\/\/${agent.serverURL}/g' ${agentPropertiesPath}

              echo "Adding env._..."
              echo "env._=${agentScriptPath}" >> ${agentPropertiesPath}

              chmod -R 770 ${paths.configuration}
              chown -R ${cfg.pathOwner} ${paths.configuration}

              echo
              echo "Result:"
              cat ${agentPropertiesPath}
            '';
          };
        }
        {
          name = "teamcity-agent-${agent.agentName}-run";
          value = {
            serviceConfig = {
              Type = "simple";
              User = agent.agentUserName;
              WorkingDirectory = "${paths.install}";
              ExecStart = "${agentScriptPath} start";
              ExecStop = "${agentScriptPath} stop";
              RemainAfterExit = "yes";
              SuccessExitStatus = "0 143";
              Restart = "always";
              RestartSec = 5;
            };
            startLimitIntervalSec = 0;
            wantedBy = [ "multi-user.target" ];
            requires = [ "teamcity-agent-${agent.agentName}-install.service" ];
            after = [ "teamcity-agent-${agent.agentName}-install.service" ];
            environment = {
              TZ = config.time.timeZone;
              TEAMCITY_CAPTURE_ENV = "${cfg.javaPackage}/bin/java -jar ${paths.install}/plugins/environment-fetcher/bin/env-fetcher.jar";
              DOTNET_HOME = pkgs.dotnet-runtime;
              DISPLAY = ":0";
              DBUS_SESSION_BUS_ADDRESS = "unix:path=/run/user/${agent.agentUserID}/bus";
              XDG_RUNTIME_DIR = "/run/user/${agent.agentUserID}";
# TODO: Proper forwarding of NIX_PATH
              NIX_PATH = "nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixos";
            } // agent.environment;
            path = with pkgs; [
              bash
              nix
              
              gawk
              zip
              git
              git-lfs
              openssh
              cfg.javaPackage
            ] ++ agent.path;
          };
        }
    ] ) cfg.agents));
  };
}
