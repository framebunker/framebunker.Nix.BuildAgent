#
# Copyright © 2023 framebunker ApS
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
# 
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

{ lib, pkgs, config, ... }:
let
  cfg = config.programs.appcenter-cli;

  scripts = {
    appcenter = pkgs.writeScriptBin "appcenter" ''
      ${pkgs.nodejs}/bin/node ${cfg.install-path}/node_modules/appcenter-cli "$@"
    '';

    appcenter-tokens-create = pkgs.writeScriptBin "appcenter-tokens-create" ''
      ${scripts.appcenter}/bin/appcenter login
      ${scripts.appcenter}/bin/appcenter tokens create
      ${scripts.appcenter}/bin/appcenter logout
    '';

    appcenter-install = pkgs.writeScriptBin "appcenter-install" ''
      echo "Installing appcenter-cli..."
      mkdir -p ${cfg.install-path}
      cd ${cfg.install-path}
      ${pkgs.nodejs}/bin/npm install appcenter-cli
      echo
      echo "Post-install test run"
      ${scripts.appcenter}/bin/appcenter
    '';
  };
in
with lib;
{
  options.programs.appcenter-cli = {
    enable = mkEnableOption "Visual Studio AppCenter cli nodeJS-based tool";
    install-path = mkOption {
      type = types.str;
      description = "Where to install the appcenter-cli tool";
      default = "/var/lib/appcenter-cli";
    };
  };

  config = mkIf cfg.enable
  {
    environment.systemPackages = [
      scripts.appcenter
      scripts.appcenter-tokens-create
    ];

    systemd.services.appcenter-cli-install = {
      serviceConfig = {
        Type = "oneshot";
        User = "root";
      };
      wantedBy = [ "multi-user.target" ];
      after = [ "network-online.target" ];
      script = ''
        wait_for_url()
        {
          echo -n "Wait for URL $1"
          while ! ${pkgs.curl}/bin/curl --output /dev/null --silent --head --fail $1; do
            sleep 0.5 && echo -n .
          done
          echo
        }
      
        testPath="${cfg.install-path}/node_modules/appcenter-cli"
        if [ -e $testPath ]; then
          echo "AppCenter install located at $testPath. No work to do here."
          exit 0
        fi

        wait_for_url https://npmjs.com

        ${scripts.appcenter-install}/bin/appcenter-install
      '';
    };
  };
}
