#
# Copyright © 2023 framebunker ApS
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
# 
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

{ lib, pkgs, config, ... }:
let
  cfg = config.programs.full-dotnet-environment;

  msBuildPath = {
    source = {
      mono = "${cfg.mono.msbuild.package}/${cfg.mono.msbuild.path}/";
      dotnet = "${cfg.dotnet.package}/${cfg.dotnet.msbuild-path}/";
    };
    destination = {
      mono = "/var/lib/msbuild-mono";
      dotnet = "/var/lib/msbuild-dotnet";
    };
  };
in
with lib;
{
  options.programs.full-dotnet-environment = {
    enable = mkEnableOption "Development environment for dotnet with mono and mono-msbuild support";
    mono = {
      package = mkOption {
        type = types.package;
        description = "The specific mono package to install";
        default = pkgs.mono;
      };
      msbuild = {
        package = mkOption {
          type = types.package;
          description = "The specific package to source mono-msbuild from";
          default = pkgs.msbuild;
        };
        path = mkOption {
          type = types.str;
          description = "Package-relative path to msbuild directory";
          default = "lib/mono/msbuild/Current/bin";
        };
      };
    };
    dotnet = {
      package = mkOption {
        type = types.package;
        description = "The specific dotnet package to install";
        default = pkgs.dotnet-sdk;
      };
      msbuild-path = mkOption {
        type = types.str;
        description = "Package-relative path to msbuild directory";
        default = "sdk/6.0.300";
      };
    };
  };

  config = mkIf cfg.enable
  {
    environment.systemPackages = [
      cfg.dotnet.package
      cfg.mono.package
    ];

    systemd.tmpfiles.rules = [
      "L+ ${msBuildPath.destination.mono} - root users - ${msBuildPath.source.mono}"
      "L+ ${msBuildPath.destination.dotnet} - root users - ${msBuildPath.source.dotnet}"
    ];
  };
}
