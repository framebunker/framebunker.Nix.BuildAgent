#
# Copyright © 2023 framebunker ApS
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
# 
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

{ lib, pkgs, config, ... }:
let
  cfg = config.programs.unity-cli;

  shellPath = "/etc/nixos/unity-cli-shell.nix";
  cliPathPostfix = "-cli";
  editorPath = "Editor";
  binaryPath = "${editorPath}/Unity";
  
  basePath = "/home/$user/Unity/Hub/Editor";
  cliPath = "$basePath/$version${cliPathPostfix}";
in
with lib;
{
  options.programs.unity-cli = {
    enable = mkEnableOption "Generate cli interfaces for Unity editors installed via the Unity Hub flatpak";
    users = mkOption {
      type = types.listOf types.str;
      description = "Users which should have their home folder scanned for Unity editor installs";
    };
  };

  config = mkIf cfg.enable
  {
    environment.systemPackages = [
      (
        pkgs.writeScriptBin "unity-cli-generate" (''
          scan_for_user()
          {
            user=$1
            basePath="${basePath}";
            
            echo "Scanning $basePath for Unity installs..."

            versions=$(find $basePath -mindepth 1 -maxdepth 1 -type d -not -name "*-cli" -exec basename {} \;)
            for version in $versions; do
              if [ -d "${cliPath}" ]; then
                echo "Skipping existing cli for $version"
              else
                generate_for_version $version "${basePath}"
              fi
            done
          }
      
          generate_for_version()
          {
            version=$1
            basePath=$2
      
            echo "Generating cli version of $version at $basePath..."
      
            scriptPath="${cliPath}/${binaryPath}";
            editorBinary="$basePath/$version/${binaryPath}";
      
            mkdir -p "${cliPath}/${editorPath}"
            cat >$scriptPath <<EOL
#! /usr/bin/env bash
echo "Launching Unity at $editorBinary with arguments [\$@] in shell ${shellPath}..."
echo
UNITY_CLI_COMMAND="$editorBinary \$@" nix-shell "${shellPath}"
EOL

            chmod +x $scriptPath
      
            echo "Completed at $scriptPath"
          }
      
          if [ ! -z $1 ]; then
            version="$1"
            user=$(whoami)
      
            generate_for_version $version "${basePath}"
          
            exit
          fi

        '' + (lib.concatStrings (builtins.map (user: 
          ''
            scan_for_user "${user}"
          '') cfg.users)))
      )
    ];
  };
}
