#
# Copyright © 2023 framebunker ApS
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
# 
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

{ config, pkgs, ... }:
let
  serverURL = ""; # Server URL without protocol - ie "ci.domain.tld:port" or "ci.local"
  agents = [
    # Agent names as strings - ie "Build-Agent-1"
  ];
in
{
  imports = [
    ./jetbrains-teamcity-agent/darwin.nix
  ];

  home.username = "build";
  home.homeDirectory = "/Users/build";

  home.stateVersion = "22.11";
  programs.home-manager.enable = true;

# NOTE: Linking /usr/local/bin to /Users/build/.nix-profile/bin for easier sub-process PATH access

  programs = {
    git = {
      enable = true;
      ignores = [ "*~" ".DS_Store" ];
      lfs.enable = true;
    };
  };

  services.jetbrains.teamcity.agent =
  let
    defaults.path = [
      (config.home.homeDirectory + "/.nix-profile/bin")
      "/nix/var/nix/profiles/default/bin"
      "/bin"
      "/usr/local/bin"
      "/usr/bin"
      "/usr/sbin"
    ];
    defaults.environment = {
      TEAMCITY_GIT_PATH = config.home.homeDirectory + "/.nix-profile/bin/git";
      NIX_PATH = "nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixpkgs";
    };
    defaults.agentConfig = name: url: {
      serverURL = url;
      agentName = name;
      path = defaults.path;
      environment = defaults.environment;
    };
  in
  {
    enable = true;
    defaultPaths.bulk = config.home.homeDirectory;
    pathOwner = config.home.username;
    agents = builtins.map (name: (defaults.agentConfig name serverURL)) agents;
  };
}
