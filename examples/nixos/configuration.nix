#
# Copyright © 2023 framebunker ApS
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
# 
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

{ config, pkgs, ... }:
let

# TODO: Auto-install Unity via flatpak
# TODO: Configure Plasma settings: Screen Locking -> Lock screen automatically: off, Energy Saving -> Screen Energy Saving: off

  users.main = {
    name = "";
    keyFiles = [];
  };

  network = {
    interface = "";
    host = "";
    sshPort = 22;
    serverURL = "";
  };

  agents = [
  	# Agent names as strings - ie "Build-Agent-1"
  ];

  timeZone = "Europe/Copenhagen";

  paths = {
    logs = "/var/log";
    temp = "/tmp";
    bulk = "/bulk";
  };
in
{
  imports = [
    ./hardware-configuration.nix
    ./terminal-preferences.nix
    ./full-dotnet-environment.nix
    ./appcenter-cli.nix
    ./unity-cli.nix
    ./jetbrains-teamcity-agent/nixos.nix
  ];

  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
    kernelParams = [
      "console=ttyS0,115200"
      "console=tty1"
    ];
    tmpOnTmpfs = true;
  };

  fileSystems."/packages".neededForBoot = true;

  zramSwap.enable = true;

  services.qemuGuest.enable = true;

  networking = {
    hostName = network.host;
    useDHCP = false;
    interfaces."${network.interface}".useDHCP = true;
  };

  time.timeZone = timeZone;
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  nixpkgs.config.allowUnfree = true;
  environment.systemPackages = with pkgs; [
    parted
    xfsprogs
    firefox    
  ];

  programs = {
    preferred-terminal.enable = true;
    java = {
      enable = true;
      package = pkgs.jetbrains.jdk;
    };
    full-dotnet-environment.enable = true;
    git = {
      enable = true;
      lfs.enable = true;
      config = {
        init.defaultBranch = "trunk";
        user = {
          name = users.main.name;
          email = "${users.main.name}@${network.host}";
        };
      };
    };
    appcenter-cli.enable = true;
    unity-cli = {
      enable = true;
      users = [ users.main.name ];
    };
  };

  services.flatpak.enable = true;
  
  services.jetbrains.teamcity.agent = {
    enable = true;
    defaultPaths.bulk = paths.bulk;
    agents = builtins.map (name: {
      serverURL = network.serverURL;
      agentName = name;
    }) agents;
  };

  users.users."${users.main.name}" = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keyFiles = users.main.keyFiles;
  };

  services.xserver = {  
    enable = true;
    layout = "us";
    xkbOptions = "eurosign:e";
    displayManager = {
      sddm.enable = true;
      autoLogin = {
        enable = true;
        user = users.main.name;
      };
    };
    desktopManager.plasma5.enable = true;
  };

  services.openssh = {
    enable = true;
    permitRootLogin = "no";
    passwordAuthentication = false;
    kbdInteractiveAuthentication = false;
    ports = [ network.sshPort ];
  };

  networking.firewall.allowedTCPPorts = config.services.openssh.ports;

  system.copySystemConfiguration = true;

  system.stateVersion = "22.05";
}
